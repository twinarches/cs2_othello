/*
 * Future improvements/notes for self:
 *
 * Iterative deepening until running out of time is very computationally redundant 
 * under current implementation, where at each move the children are cleared.
 * However, without clearing, children are very memory expensive. 
 *
 * A transposition table would have helped a little bit because it will enable 
 * the iterative deepening process to at least start at the first unknown depth
 * (i.e. at a given tree that has already been traversed to 6 ply, T.T. will
 * enable us to start immediately at 7-ply in the iterative deepening process)
 * This is projected to give little benefit because additional ply are
 * significantly more expensive that the original ply and so will only provide
 * little speedup. Stronger cases for T.T. improving computations is given below
 *
 * Furthermore, in future implementations, it is recommended to use transposition
 * tables to change move ordering in subsequent computations at deeper ply to
 * improve pruning rates; a further possible improvement is to adopt Negascout
 * with the T.T. helping find the principal variation with which to conduct null
 * window search.
 *
 * T.T. could also keep track of won positions so that a position once computed
 * to be won would not have to be recomputed. This would allow an endgame-suited
 * T.T. without separately recoding one, as in the endgame it is advantageous to
 * use a T.T. because numerous transpositions + no ''depth'' issue if we instead
 * of hashing score hash ''win'' or ''loss'' and a lookup to the optimal line.
 *
 * Move cuts could play a dramatic role in speeding up computations; lookahead
 * time for 7-ply is usually on the order of 20s, so using a 4/10 cut with
 * keeping ~20ish moves on 4 could easily improve our lookahead to 10-ply faster
 * than we currently do 7-ply. 4/12 is a standard cut but we can't accomplish.
 *
 * An additional way to speed up computations is to choose smartly; if the same
 * move comes out ahead after mulitple iterative deepenings and always by a
 * decent margin, we should choose that move!
 *
 * Lastly of course we can find a way to adjust our heuristic come endgame, and
 * also use a fastest-first ordering instead of a best-first ordering (effectively
 * improves pruning when we're searching only for win/loss).
 *
 * In terms of raw improvements that's about it. Hopefully we can come back
 * someday and finish this!!
 *
 * - Yubo, coder of the unfinished half of this project.
 */
#include "player.h"
#include <ctime>
#include <cmath>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) 
{
    fprintf(stderr, "\nTwin Arches: GLHF\n\n");
    Board *board = new Board();
    root = new Node(board, (Side)(1 - side));
    mySide = side;
    opening = true;
    book = new PlayBook();
}

/*
 * Destructor for the player.
 */
Player::~Player() {
    fprintf(stderr, "\nGGWP!\n\n");
    delete root;
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) 
{
    if (msLeft == -1) 
    {
        msLeft = 5000;//random value, 5s
    }
    Node * newRoot;
    if (opponentsMove != NULL) 
    {
        newRoot = new Node(root, opponentsMove->toInt());
    }
    else 
    {
        /* nprintf(stderr, "hi\n"); */
        newRoot = new Node(root, NOMOVE);
    }
    delete root;
    root = newRoot;

    int bestMove = NOMOVE;
    
    //if opening book
    if (opening) 
    {
        //get opening move
        if (opponentsMove == NULL) 
        {
            bestMove = book->getMove(-1);
        }
        else
        {
            bestMove = book->getMove(opponentsMove->toInt());
        }

        //parse opening move
        if (bestMove != -1) 
        {
            newRoot = new Node(root, bestMove);
            delete root;
            root = newRoot;

            /* return move */
            return new Move(getcol(bestMove), getrow(bestMove));
        }
        else 
        {
            delete book;
            opening = false;
        }
    }

    SCORETYPE val;
    SCORETYPE bestScore = 0;
    int projectedTime = 0;
    int prevTime = 0;
    int newTime = 1;
    int depth = 3;
    int start, end;

    double scale = 20 * (1 - pow((root->board->countWhite() + root->board->countBlack()) / 64.0, 1.2)) + 1;
    while(projectedTime < (int)(msLeft / scale))
    {
        bestScore = -INFINITY;
        //get start time
        start = clock() / (CLOCKS_PER_SEC / 1000);
        /* alpha beta prune possible children */
        for (int i = 0; i < 64; i++) 
        {
            if (root->moves[i]) 
            {
                newRoot = new Node(root, i);
                val = -newRoot->negamax(-INFINITY, -bestScore, depth);
                if (val >= bestScore) 
                {
                    bestScore = val;
                    bestMove = i;
                }
                delete newRoot;
            }
        }
        //get endTime
        end = clock() / (CLOCKS_PER_SEC / 1000);

        //compute projected Time
        prevTime = newTime;
        newTime = end - start;
        msLeft -= newTime;
        /* fixed bug; program would not seek ahead in endgame because newTime
         * was too small; we still want to leave the catch in though */
        if (depth > (64 - (root->board->countWhite() + root->board->countBlack())) || (newTime <= prevTime && newTime > 8)) 
        {
            break;
        }
        /* fprintf(stderr, "%d\n", newTime); */
        projectedTime = newTime * (newTime / prevTime);
        depth++;
        fprintf(stderr, "Took %dms to search%d ply, projected %d/%dms for %d ply\n",
                newTime, depth - 1, projectedTime, msLeft, depth);
    }
    fprintf(stderr, "Current score is %f\n", bestScore);
    if (bestMove == NOMOVE) 
    {
        newRoot = new Node(root, NOMOVE);
        delete root;
        root = newRoot;
        return NULL;
    }
    else 
    {
        newRoot = new Node(root, bestMove);
        delete root;
        root = newRoot;
        return new Move(getcol(bestMove), getrow(bestMove));
    }
}
