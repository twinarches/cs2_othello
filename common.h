#ifndef __COMMON_H__
#define __COMMON_H__

#include <cstdlib>

#define NUM_ROWS 8
#define NUM_COLS 8

#define getrow(idx) (char)((idx) / NUM_COLS)
#define getcol(idx) (char)((idx) % NUM_COLS)
#define getidx(row, col) (char)((row) * NUM_COLS + (col))

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) < (y) ? (y) : (x))

enum Side {
    WHITE, BLACK
};

class Move {

public:
    int x, y;
    Move(int x, int y) {
        this->x = x;
        this->y = y;
    }
    ~Move() {}

    int getX() { return x; }
    int getY() { return y; }

    void setX(int x) { this->x = x; }
    void setY(int y) { this->y = y; }
    int toInt() { return getidx(y, x); }
};

#endif
