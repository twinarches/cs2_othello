#include "board.h"
#include "moves.h"


char diashift[15] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    1, 2, 3, 4, 5, 6, 7,
};

unsigned char diamask[15] = {
    0x01, 0x03, 0x07, 0x0f, 0x1f, 0x3f, 0x7f, 0xff,
    0x7f, 0x3f, 0x1f, 0x0f, 0x07, 0x03, 0x01,
};

char R90map[64] = {
     0,  8, 16, 24, 32, 40, 48, 56,
     1,  9, 17, 25, 33, 41, 49, 57,
     2, 10, 18, 26, 34, 42, 50, 58,
     3, 11, 19, 27, 35, 43, 51, 59,
     4, 12, 20, 28, 36, 44, 52, 60,
     5, 13, 21, 29, 37, 45, 53, 61,
     6, 14, 22, 30, 38, 46, 54, 62,
     7, 15, 23, 31, 39, 47, 55, 63
};

char R45map[64] = {
     0,  9, 18, 27, 36, 45, 54, 63,
     8, 17, 26, 35, 44, 53, 62,  7,
    16, 25, 34, 43, 52, 61,  6, 15,
    24, 33, 42, 51, 60,  5, 14, 23,
    32, 41, 50, 59,  4, 13, 22, 31,
    40, 49, 58,  3, 12, 21, 30, 39,
    48, 57,  2, 11, 20, 29, 38, 47,
    56,  1, 10, 19, 28, 37, 46, 55,
};

char L45map[64] = {
    56, 48, 40, 32, 24, 16,  8,  0,
     1, 57, 49, 41, 33, 25, 17,  9,
    10,  2, 58, 50, 42, 34, 26, 18,
    19, 11,  3, 59, 51, 43, 35, 27,
    28, 20, 12,  4, 60, 52, 44, 36,
    37, 29, 21, 13,  5, 61, 53, 45,
    46, 38, 30, 22, 14,  6, 62, 54,
    55, 47, 39, 31, 23, 15,  7, 63,
};


/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board()
{
    placeWhite(getidx(3, 3));
    placeWhite(getidx(4, 4));
    placeBlack(getidx(3, 4));
    placeBlack(getidx(4, 3));
}

/*
 * Destructor for the board.
 */
Board::~Board()
{
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy()
{
    Board *newBoard = new Board();

    newBoard->blackR0 = blackR0;
    newBoard->blackR90 = blackR90;
    newBoard->blackR45 = blackR45;
    newBoard->blackL45 = blackL45;
    newBoard->whiteR0 = whiteR0;
    newBoard->whiteR90 = whiteR90;
    newBoard->whiteR45 = whiteR45;
    newBoard->whiteL45 = whiteL45;

    return newBoard;
}

inline bool Board::occupied(char x, char y)
{
    return (blackR0[x + 8*y] || whiteR0[x + 8*y]);
}

/*
 * Returns true if the game is finished; false otherwise. The game is finished
 * if neither side has a legal move.
 */
inline bool Board::isDone()
{
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
inline bool Board::hasMoves(Side side)
{
    return (getLegalMoves(side).any()) ? true : false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(int m, Side side)
{
    int i = m - 1;
    int lim = 8*(m/8);


    if (side == WHITE)
    {
        placeWhite(m);

        /* check row before 'm' */
        while (i > lim && blackR0[i]) { i--; }
        if (i >= 0 &&whiteR0[i]) {
            for (; i < m; i++)
                placeWhite(i);
        }
        /* check row after 'm' */
        i = m + 1;
        lim += 7;
        while (i < lim && blackR0[i]) { i++; }
        if (i <= 63 && whiteR0[i]) {
            for (; i > m; i--)
                placeWhite(i);
        }

        /* check column before 'm' */
        i = m - 8;
        lim = m%8;
        while (i > lim && blackR0[i]) { i -= 8; }
        if (i >= 0 &&whiteR0[i]) {
            for (; i < m; i += 8)
                placeWhite(i);
        }
        /* check column after 'm' */
        i = m + 8;
        lim += 56;
        while (i < lim && blackR0[i]) { i += 8; }
        if (i <= 63 && whiteR0[i]) {
            for (; i > m; i -= 8)
                placeWhite(i);
        }

        /* check diagonal 1 before 'm' */
        i = m - 9;
        lim = (m/8 > m%8) ? 8*(m/8 - m%8) : m%8 - m/8;
        while (i > lim && blackR0[i]) { i -= 9; }
        if (i >= 0 &&whiteR0[i]) {
            for (; i < m; i += 9)
                placeWhite(i);
        }
        /* check diagonal 1 after 'm' */
        i = m + 9;
        lim = (m/8 > m%8) ? 63 - (m/8 - m%8) : 63 - 8*(m%8 - m/8);
        while (i < lim && blackR0[i]) { i += 9; }
        if (i <= 63 && whiteR0[i]) {
            for (; i > m; i -= 9)
                placeWhite(i);
        }

        /* check diagonal 2 before 'm' */
        i = m - 7;
        lim = (m/8 + m%8 < 8) ? m/8 + m%8 : 8*((m/8 + m%8)-8) + 15;
        while (i > lim && blackR0[i]) { i -= 7; }
        if (i >= 0 &&whiteR0[i]) {
            for (; i < m; i += 7)
                placeWhite(i);
        }
        /* check diagonal 2 after 'm' */
        i = m + 7;
        lim = (m/8 + m%8 < 8) ? 8*(m/8 + m%8) : 49 + m/8 + m%8;
        while (i < lim && blackR0[i]) { i += 7; }
        if (i <= 63 && whiteR0[i]) {
            for (; i > m; i -= 7)
                placeWhite(i);
        }
    }
    else
    {
        placeBlack(m);

        /* check row before 'm' */
        while (i > lim && whiteR0[i]) { i--; }
        if (i >= 0 &&blackR0[i]) {
            for (; i < m; i++)
                placeBlack(i);
        }
        /* check row after 'm' */
        i = m + 1;
        lim += 7;
        while (i < lim && whiteR0[i]) { i++; }
        if (i <= 63 && blackR0[i]) {
            for (; i > m; i--)
                placeBlack(i);
        }

        /* check column before 'm' */
        i = m - 8;
        lim = m%8;
        while (i > lim && whiteR0[i]) { i -= 8; }
        if (i >= 0 &&blackR0[i]) {
            for (; i < m; i += 8)
                placeBlack(i);
        }
        /* check column after 'm' */
        i = m + 8;
        lim += 56;
        while (i < lim && whiteR0[i]) { i += 8; }
        if (i <= 63 && blackR0[i]) {
            for (; i > m; i -= 8)
                placeBlack(i);
        }

        /* check diagonal 1 before 'm' */
        i = m - 9;
        lim = (m/8 > m%8) ? 8*(m/8 - m%8) : m%8 - m/8;
        while (i > lim && whiteR0[i]) { i -= 9; }
        if (i >= 0 &&blackR0[i]) {
            for (; i < m; i += 9)
                placeBlack(i);
        }
        /* check diagonal 1 after 'm' */
        i = m + 9;
        lim = (m/8 > m%8) ? 63 - (m/8 - m%8) : 63 - 8*(m%8 - m/8);
        while (i < lim && whiteR0[i]) { i += 9; }
        if (i <= 63 && blackR0[i]) {
            for (; i > m; i -= 9)
                placeBlack(i);
        }

        /* check diagonal 2 before 'm' */
        i = m - 7;
        lim = (m/8 + m%8 < 8) ? m/8 + m%8 : 8*((m/8 + m%8)-8) + 15;
        while (i > lim && whiteR0[i]) { i -= 7; }
        if (i >= 0 &&blackR0[i]) {
            for (; i < m; i += 7)
                placeBlack(i);
        }
        /* check diagonal 2 after 'm' */
        i = m + 7;
        lim = (m/8 + m%8 < 8) ? 8*(m/8 + m%8) : 49 + m/8 + m%8;
        while (i < lim && whiteR0[i]) { i += 7; }
        if (i <= 63 && blackR0[i]) {
            for (; i > m; i -= 7)
                placeBlack(i);
        }
    }
}

inline void Board::placeWhite(int m)
{
    whiteR0[m] = 1;
    whiteR90[R90map[m]] = 1;
    whiteR45[R45map[m]] = 1;
    whiteL45[L45map[m]] = 1;
    blackR0[m] = 0;
    blackR90[R90map[m]] = 0;
    blackR45[R45map[m]] = 0;
    blackL45[L45map[m]] = 0;
}

inline void Board::placeBlack(int m)
{
    blackR0[m] = 1;
    blackR90[R90map[m]] = 1;
    blackR45[R45map[m]] = 1;
    blackL45[L45map[m]] = 1;
    whiteR0[m] = 0;
    whiteR90[R90map[m]] = 0;
    whiteR45[R45map[m]] = 0;
    whiteL45[L45map[m]] = 0;
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side)
{
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack()
{
    return blackR0.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite()
{
    return whiteR0.count();
}

/*
 * Sets the board state given an 8x8 char array where 'w' indicates a white
 * piece and 'b' indicates a black piece. Mainly for testing purposes.
 */
void Board::setBoard(char data[])
{
    blackR0.reset();
    blackR90.reset();
    blackR45.reset();
    blackL45.reset();
    whiteR0.reset();
    whiteR90.reset();
    whiteR45.reset();
    whiteL45.reset();

    for (int i = 0; i < 64; i++)
    {
        if (data[i] == 'b')
        {
            placeBlack(i);
        } if (data[i] == 'w')
        {
            placeWhite(i);
        }
    }
}

/*
 * Get all the legal moves for the board. The returned bitset contains a 1 for
 * every move that can legally be made for the specified player.
 */
bitset<64> Board::getLegalMoves(Side side)
{
    bitset<64> out;
    bitset<8> rows[8];
    bitset<8> cols[8];
    bitset<8> diag1[15];
    bitset<8> diag2[15];

    /* get legal moves for each row, column, and diagonal */
    for (int i = 0; i < 8; i++)
    {
        rows[i] = bitset<8>(moves[(whiteR0.to_ulong() >> 8*i) & 0xff]
                [(blackR0.to_ulong() >> 8*i) & 0xff][side]);
        cols[i] = bitset<8>(moves[(whiteR90.to_ulong() >> 8*i) & 0xff]
                [(blackR90.to_ulong() >> 8*i) & 0xff][side]);
    }
    for (int i = 2; i < 13; i++)
    {
        unsigned long tmpW = (whiteR45.to_ulong() >> 8*(i%8)) >> diashift[i];
        tmpW &= diamask[i];
        unsigned long tmpB = (blackR45.to_ulong() >> 8*(i%8)) >> diashift[i];
        tmpB &= diamask[i];
        diag1[i] = bitset<8>(moves[tmpW][tmpB][side]);
        tmpW = (whiteL45.to_ulong() >> 8*(i%8)) >> diashift[i];
        tmpW &= diamask[i];
        tmpB = (blackL45.to_ulong() >> 8*(i%8)) >> diashift[i];
        tmpB &= diamask[i];
        diag2[i] = bitset<8>(moves[tmpW][tmpB][side]);
    }

    for (int i = 0, k = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++, k++)
        {
            out.set(k, rows[i][j] || cols[j][i] || diag1[i+j][min(j, 7-i)]
                    || diag2[7+i-j][min(i, j)]);
        }
    }

    return out;
}

void Board::printBitset(bitset<64> board)
{
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (board[getidx(i, j)])
                fprintf(stderr, "1");
            else
                fprintf(stderr, "0");
        }
        fprintf(stderr, "\n");
    }
    fprintf(stderr, "\n");
}

void Board::printBoard()
{
    printBitset(whiteR0);
    printBitset(blackR0);
}

double Board::heuristic(Side s)
{
    double total = 0;
    double wp = countWhite();
    double bp = countBlack();
    double scores[6];
    double weights[6] = { 10, 801.724, 382.026, 78.922, 74.396, 10, };
    int discs[64] = {
        20, -3, 11, 8, 8, 11, -3, 20,
        -3, -7, -4, 1, 1, -4, -7, -3,
        11, -4, 2, 2, 2, 2, -4, 11,
        8, 1, 2, -3, -3, 2, 1, 8,
        8, 1, 2, -3, -3, 2, 1, 8,
        11, -4, 2, 2, 2, 2, -4, 11,
        -3, -7, -4, 1, 1, -4, -7, -3,
        20, -3, 11, 8, 8, 11, -3, 20,
    };
    int i;
    bitset<64> mask = bitset<64>(460039);
    bitset<64> mask2;

    /* simple piece count */
    scores[0] = (wp == bp) ? 0 : ((wp > bp) ? -100.0 * (wp / (wp + bp))
            : 100.0 * (bp / (wp + bp)));

    /* corner occupancy */
    scores[1] = 25.0 * (blackR0[0] + blackR0[7] + blackR0[56] + blackR0[63])
            - 25.0 * (whiteR0[0] + whiteR0[7] + whiteR0[56] + whiteR0[63]);

    /* corner adjacency */
    scores[2] = 12.5 * ((!blackR0[0] && !whiteR0[0])*(whiteR0[1] + whiteR0[8]
                - blackR0[1] - blackR0[8]) + (!blackR0[7] && !whiteR0[7])
                *(whiteR0[6] + whiteR0[15] - blackR0[6] - blackR0[15])
                + (!blackR0[56] && !whiteR0[56])*(whiteR0[48] + whiteR0[57]
                - blackR0[48] - blackR0[57]) + (!blackR0[63] && !whiteR0[63])
                *(whiteR0[55] + whiteR0[62] - blackR0[55] - blackR0[62]));

    /* mobility */
    wp = getLegalMoves(WHITE).count();
    bp = getLegalMoves(BLACK).count();
    scores[3] = (wp == bp) ? 0 : ((wp > bp) ? -100.0 * (wp / wp + bp)
            : 100.0 * (bp / (wp + bp)));

    /* frontier squares */
    bp = 0;
    wp = 0;

    /* this loop checks the inner squares of the board */
    for (i = 1; i < 7; i++)
    {
        for (int j = 1; j < 7; j++)
        {
            bitset<64> frontier = ~(blackR0 | whiteR0) & mask;
            bp += (blackR0[getidx(i, j)] && frontier.any());
            wp += (whiteR0[getidx(i, j)] && frontier.any());
            mask <<= 1;
        }
        mask <<= 2;
    }
    mask = bitset<64>(1797);
    mask2 = bitset<64>(362258295026614272ul);

    /* this loop checks the edge squares of the board */
    for (i = 1; i < 7; i++)
    {
        /* check top row/first col */
        bitset<64> frontier1 = ~(blackR0 | whiteR0) & mask;
        bitset<64> frontier2 = ~(blackR90 | whiteR90) & mask;
        bp += (blackR0[getidx(0, i)] && frontier1.any());
        bp += (blackR90[getidx(0, i)] && frontier2.any());
        wp += (whiteR0[getidx(0, i)] && frontier1.any());
        wp += (whiteR90[getidx(0, i)] && frontier2.any());

        /* check bottom row/last col */
        frontier1 = ~(blackR0 | whiteR0) & mask2;
        frontier2 = ~(blackR90 | whiteR90) & mask2;
        bp += (blackR0[getidx(7, i)] && frontier1.any());
        bp += (blackR90[getidx(7, i)] && frontier2.any());
        wp += (whiteR0[getidx(7, i)] && frontier1.any());
        wp += (whiteR90[getidx(7, i)] && frontier2.any());

        mask <<= 1;
        mask2 <<= 1;
    }
    scores[4] = (bp == wp) ? 0 :
        ((bp > wp) ? -100.0 * bp / (bp + wp) : 100.0 * wp / (bp + wp));

    /* disc squares */
    scores[5] = 0;
    for (i = 0; i < 64; i++)
    {
        scores[5] += discs[i] * blackR0[i];
        scores[5] -= discs[i] * whiteR0[i];
    }
    for (i = 0; i < 6; i++)
    {
        total += scores[i] * weights[i];
    }
    return (s == WHITE) ? -total : total;
}
