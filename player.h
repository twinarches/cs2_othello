#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
#include "playbook.h"
#include "Node.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
private:
    Node * root;
    bool opening;
    PlayBook * book;
    Side mySide;
};

#endif
