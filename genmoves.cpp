#include <cstdio>
#include <bitset>

using namespace std;

char getmoves(int white, int black)
{
    bitset<8> w = bitset<8>(white);
    bitset<8> b = bitset<8>(black);
    bool wl[8];
    bool wr[8];
    bitset<8> out;

    bool wa = false;
    for (int i = 0; i < 8; i++)
    {
        wl[i] = wa;
        if (w[i])
            wa = true;
        else if (!b[i])
            wa = false;
    }
    wa = false;
    for (int i = 0; i < 8; i++)
    {
        wr[7 - i] = wa;
        if (w[7 - i])
            wa = true;
        else if (!b[7-i])
            wa = false;
    }

    if (wr[0] && b[1] && !b[0] && !w[0])
    {
        out.set(0);
    }
    for (int i = 1; i < 7; i++)
    {
        if (!b[i] && !w[i] && ((wr[i] && b[i+1]) || (wl[i] && b[i-1])))
            out.set(i);
    }
    if(wl[7] && b[6] && !b[7] && !w[7])
        out.set(7);

    return (unsigned char)out.to_ulong();
}

int main(int argc, char **argv)
{
    int i;
    int j;
    unsigned char moves[256][256][2];

    for (i = 0; i < 256; i++)
    {
        for (j = 0; j < 256; j++)
        {
            moves[i][j][0] = getmoves(i, j);
            moves[i][j][1] = getmoves(j, i);
        }
    }

    printf("#ifndef __MOVES_H__\n#define __MOVES_H__\n\n\n");
    printf("unsigned char moves[256][256][2] = {\n");
    for (i = 0; i < 256; i++)
    {
        printf("    {\n");
        for (j = 0; j < 256; j++)
        {
            printf("        {(unsigned char)%d, (unsigned char)%d},\n",
                    moves[i][j][0], moves[i][j][1]);
        }
        printf("    },\n");
    }
    printf("};\n");
    printf("\n\n#endif /* __MOVES_H__ */\n");
    return 0;
}
