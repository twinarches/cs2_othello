#include "Node.h"

/* Constructor, takes board state and parent pointer */
Node::Node (Board * bd, Side s)
{
    /* fprintf(stderr, "let there be light\n"); */
    board = bd;
    moves = board->getLegalMoves(s);
    side = s;
}

/* constructor for child node */
Node::Node(Node * node, int move)
{
    /* fprintf(stderr, "let there be light\n"); */
    board = node->board->copy();
    if(move != NOMOVE)
    {
        board->doMove(move, node->side);
    }

    side = (Side)(1 - node->side);
    moves = board->getLegalMoves(side);
}

/* Destructor; make sure to free all memory! */
Node::~Node ()
{
    delete board;
}


/* simple negamax */
SCORETYPE Node::negamax(SCORETYPE alpha, SCORETYPE beta, int depth)
{
    if (board->count(side) == 0) 
    {
        return -INFINITY;
    }
    else if(board->countWhite() + board->countBlack() == 64 || 
            (board->getLegalMoves(side).none() && 
             (board->getLegalMoves((Side)(1 - side)).none())))
    {
        return board->count(side) - board->count((Side)(1 - side));
    }
    /* base case */
    if (depth <= 0) 
    {
        return heuristic();
    }

    SCORETYPE retVal;

    /* recursive case */

    Node * child;
    SCORETYPE val = -INFINITY;
    if (moves.none()) 
    {
        child = new Node(this, NOMOVE);
        val = -child->negamax(-beta, INFINITY, depth - 1);
    }
    else
    {
        for (int i = 0; i < 64; i++) 
        {
            if (moves[i]) 
            {
                child = new Node(this, i);
                retVal = -child->negamax(-beta, -val, depth - 1);
                val = max(val, retVal);
                delete child;

                if(val > beta)
                {
                    break;
                }
            }
        }
    }
    return val;
}
