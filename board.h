#ifndef __BOARD_H__
#define __BOARD_H__


#include <bitset>
#include <cstdio>
#include "common.h"

using namespace std;

class Board {

private:
    bitset<64> blackR0;
    bitset<64> blackR90;
    bitset<64> blackR45;
    bitset<64> blackL45;
    bitset<64> whiteR0;
    bitset<64> whiteR90;
    bitset<64> whiteR45;
    bitset<64> whiteL45;

    bool occupied(char x, char y);
    void placeWhite(int m);
    void placeBlack(int m);

public:
    Board();
    ~Board();
    Board *copy();

    bool isDone();
    bool hasMoves(Side side);
    void doMove(int m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
    double heuristic(Side s);

    void setBoard(char data[]);

    bitset<64> getLegalMoves(Side side);
    void printBitset(bitset<64> board);
    void printBoard();
};


#endif /* __BOARD_H__ */
