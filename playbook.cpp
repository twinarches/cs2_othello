#include "playbook.h"

const int BUFFER = 256;


PlayBook::PlayBook()
{
    sqlite3_open("playbook.db", &db);
    tbl = trans1;
    mvpattern[0] = '\0';
    idx = 0;
}

PlayBook::PlayBook(char *filename)
{
    sqlite3_open(filename, &db);
    tbl = trans1;
    mvpattern[0] = '\0';
    idx = 0;
}

PlayBook::~PlayBook()
{
    sqlite3_close(db);
}

/*
 * Gets the next move from the playbook. Assumes that the passed move is
 * valid. If the player passes, then the line of play is not in the playbook,
 * so don't even bother passing an invalid move. Always remember to call
 * firstMove with the first move of the game instead of this function.
 */
int PlayBook::getMove(int m)
{
    char sqlstmt[BUFFER];
    char out[3] = {'\0', '\0', '\0', };
    sqlite3_stmt *query;
    int rc;
    const unsigned char *newpattern;
    int ret;

    if (idx == 0)
        return firstMove(m);
    else if (m == -1 || idx == 118)
        return -1;

    toStr(m, out);
    strcat(mvpattern, out);
    idx += 2;

    sprintf(sqlstmt, "SELECT line FROM plays WHERE line LIKE '%s%%';",
            mvpattern);
    sqlite3_prepare(db, sqlstmt, BUFFER, &query, NULL);
    rc = sqlite3_step(query);
    if (rc == SQLITE_ROW)
    {
        newpattern = sqlite3_column_text(query, 0);
        out[0] = mvpattern[idx] = newpattern[idx];
        idx++;
        out[1] = mvpattern[idx] = newpattern[idx];
        idx++;
        out[2] = mvpattern[idx] = '\0';

        ret = toInt(out);
    }
    else if (rc == SQLITE_DONE) 
    {
        fprintf(stderr, "SQLITE DONE\n");
        ret = -1;
    }
    else
    {
        fprintf(stderr, "SQLITE *ERROR*!!!\n");
        ret = -1;
    }

    sqlite3_finalize(query);
    return ret;
}

int PlayBook::firstMove(int m)
{
    if (m == -1)
    {
        mvpattern[0] = 'F';
        mvpattern[1] = '5';
        mvpattern[2] = '\0';
        idx = 2;

        return 37;
    }
    else
    {
        switch (m)
        {
        case 37:
            tbl = trans1;
            break;
        case 44:
            tbl = trans2;
            break;
        case 19:
            tbl = trans3;
            break;
        case 26:
            tbl = trans4;
        }
        mvpattern[0] = 'F';
        mvpattern[1] = '5';
        mvpattern[2] = 'F';
        mvpattern[3] = '6';
        mvpattern[4] = '\0';
        idx = 4;

        return tbl[45];
    }
}

inline int PlayBook::toInt(char *in)
{
    int ret = (in[0] - 'A') + 8*(in[1] - '1');
    return tbl[ret];
}

inline void PlayBook::toStr(int m, char *out)
{
    int ret = tbl[m];
    out[0] = (ret % 8) + 'A';
    out[1] = (ret / 8) + '1';
}
