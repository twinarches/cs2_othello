#ifndef __NODE_H__
#define __NODE_H__

#include <vector>
#include "board.h"
#include <bitset>
#include <cfloat>
#include "common.h"
#include <climits>

#define NOMOVE -1
#define MEMOPT

/* defines types of scores and thus alpha-beta bounds */
typedef double SCORETYPE;
#define INFINITY DBL_MAX

class Node 
{
    public:
        /* Constructor, takes board state and parent pointer */
        Node (Board * board, Side s);

        /* Constructor to make child node */
        Node (Node * node, int move);

        /* Destructor; make sure to free all memory! */
        ~Node ();

        /* helper func */
        inline bitset<64> getLegalMoves(){ return moves; }

        /* heuristic */
        inline double heuristic(){ return board->heuristic(side); }

        /* print board */
        inline void printBoard(){ board->printBoard(); }

        /* simple negamax */
        SCORETYPE negamax(SCORETYPE alpha, SCORETYPE beta, int depth);

/* private: */
        Board * board;
        bitset<64> moves;
        Side side;
};
#endif
